<?php

declare(strict_types = 1);

namespace Drupal\umami_search_web_components_adv\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a search component: facet checkbox.
 *
 * @Block(
 *   id = "uswca_search_example",
 *   admin_label = @Translation("Umami Search Component: Example"),
 *   category = @Translation("Search Components"),
 * )
 */
final class SearchExampleBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'displayText' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state): array {
    $form['displayText'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Text to display'),
      '#default_value' => $this->configuration['displayText'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state): void {
    $this->configuration['displayText'] = $form_state->getValue('displayText');
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    return [
      '#theme' => 'uswca_search_example',
      '#displayText' => $this->configuration['displayText'],
    ];
  }

}
