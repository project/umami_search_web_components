import {BaseSearchElement} from "drupal-search-web-components/dist/BaseSearchElement.js";
import {customElement, property} from 'lit/decorators.js';
import {html} from "lit";
import {unsafeHTML} from 'lit/directives/unsafe-html.js';

export interface RecipeData {
  author?: string,
  image?: string,
  cooking_time?: string,
  difficulty?: string,
  preparation_time?: string,
  recipe_category?: string,
  servings?: string,
  title?: string,
  url?: string,
  summary?: string,
}

@customElement('search-result-recipe')
export class SearchResultRecipe extends BaseSearchElement {

  @property({type: Object})
  data: RecipeData = {};

  @property({type: Object})
  settings = {};

  override render() {
    return html`
      <div>

        <div class="details-img">
          <div class="image">
            <img src="${this.data.image}"/>
          </div>
          <div class="details">
            <h2>
              <a href="${this.data.url}" rel="bookmark">
                <span class="field field--name-title">${this.data.title}</span>
              </a>
            </h2>
            <ul class="info">
              ${this.data.servings ? html`<li><span class="label"><strong>Servings: </strong></span><span class="value">${this.data.servings}</span></li>` : null}
              ${this.data.servings ? html`<li><span class="label"><strong>Difficulty: </strong></span><span class="value">${this.data.difficulty}</span></li>` : null}
              ${this.data.servings ? html`<li><span class="label"><strong>Prep time: </strong></span><span class="value">${this.data.preparation_time}min</span></li>` : null}
              ${this.data.servings ? html`<li><span class="label"><strong>Cooking time: </strong></span><span class="value">${this.data.cooking_time}min</span></li>` : null}
            </ul>
            <div class="summary">
              ${unsafeHTML(this.data.summary)}
            </div>
            <div class="providedby">Provided by <a href="https://git.drupalcode.org/project/umami_search_web_components/-/blob/1.0.x/modules/umami_search_web_components_adv/lit/src/search-result-recipe/search-result-recipe.ts">umami_search_web_components_adv</a></div>
          </div>
        </div>
      </div>
    `;
  }
}

declare global {
  interface HTMLElementTagNameMap {
    'search-result-recipe': SearchResultRecipe;
  }
}
