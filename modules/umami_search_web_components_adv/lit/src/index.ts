import {SearchRoot} from "drupal-search-web-components/dist/search-root/search-root.js"
import {FacetCheckbox} from "drupal-search-web-components/dist/facet-checkbox/facet-checkbox";
import {FacetDropdown} from "drupal-search-web-components/dist/facet-dropdown/facet-dropdown";
import {FacetDropdownHtml} from "drupal-search-web-components/dist/facet-dropdown-html/facet-dropdown-html";
import {SearchAppliedFacets} from "drupal-search-web-components/dist/search-applied-facets/search-applied-facets";
import {SearchBox} from "drupal-search-web-components/dist/search-box/search-box";
import {SearchInput} from "drupal-search-web-components/dist/search-input/search-input";
import {SearchNoResultsMessage} from "drupal-search-web-components/dist/search-no-results-message/search-no-results-message";
import {SearchResultElementDefault} from "drupal-search-web-components/dist/search-result-element-default/search-result-element-default";
import {SearchResultElementRendered} from "drupal-search-web-components/dist/search-result-element-rendered/search-result-element-rendered";
import {SearchResultSummary} from "drupal-search-web-components/dist/search-result-summary/search-result-summary";
import {SearchResults} from "drupal-search-web-components/dist/search-results/search-results";
import {SearchResultsPerPage} from "drupal-search-web-components/dist/search-results-per-page/search-results-per-page";
import {SearchSimplePager} from "drupal-search-web-components/dist/search-simple-pager/search-simple-pager";
import {SearchSort} from "drupal-search-web-components/dist/search-sort/search-sort";
import {SearchExample} from "./search-example/search-example";
import {SearchResultRecipe} from "./search-result-recipe/search-result-recipe";
import {FacetButton} from "drupal-search-web-components/dist/facet-button/facet-button";
import {SearchResultsSwitcher} from "drupal-search-web-components/dist/search-results-switcher/search-results-switcher";

export default {
  SearchRoot,
  FacetButton,
  FacetCheckbox,
  FacetDropdown,
  FacetDropdownHtml,
  SearchAppliedFacets,
  SearchBox,
  SearchInput,
  SearchNoResultsMessage,
  SearchResultElementDefault,
  SearchResultElementRendered,
  SearchResultSummary,
  SearchResults,
  SearchResultsPerPage,
  SearchResultsSwitcher,
  SearchSimplePager,
  SearchSort,
  SearchExample,
  SearchResultRecipe
}
