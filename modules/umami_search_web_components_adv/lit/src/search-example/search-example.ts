import {BaseSearchElement} from "drupal-search-web-components/dist/BaseSearchElement.js";
import {customElement, property} from 'lit/decorators.js';
import {html} from "lit";

@customElement('search-example')
export class SearchExample extends BaseSearchElement {

  @property()
  displayText = 'This is some example display text';

  override render() {
    if (!this.context?.response) {
      return null;
    }

    return html`
      <div>
        <h2>Example advanced component</h2>
        <div>${this.displayText}</div>
        <div class="providedby">Provided by <a href="https://git.drupalcode.org/project/umami_search_web_components/-/blob/1.0.x/modules/umami_search_web_components_adv/lit/src/search-example/search-example.ts">umami_search_web_components_adv</a></div>
      </div>
    `;
  }
}

declare global {
  interface HTMLElementTagNameMap {
    'search-example': SearchExample;
  }
}
