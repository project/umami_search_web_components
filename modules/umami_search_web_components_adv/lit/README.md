# Drupal Search Web Components

The bundled and module javascript for [search_web_components](https://www.drupal.org/project/search_web_components).

A set of lit web components built on [search_api_decoupled](https://www.drupal.org/project/search_api_decoupled) endpoints
to provide a fully featured search experience.

# Install
@TODO

# Example
@TODO

# Usage
@TODO
