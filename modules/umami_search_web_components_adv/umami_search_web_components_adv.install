<?php

/**
 * @file
 * Primary module hooks for umami_search_web_components_adv module.
 */

use Drupal\Core\Url;
use Drupal\layout_builder\SectionComponent;

/**
 * Implements hook_theme().
 */
function umami_search_web_components_adv_install() {
  $params = Url::fromUserInput('/search')->getRouteParameters();
  $searchNode = \Drupal::entityTypeManager()->getStorage('node')->load($params['node']);

  $endpoint = \Drupal::configFactory()->getEditable('search_api_decoupled.search_api_endpoint.content');
  $config = $endpoint->get('third_party_settings');

  $config['search_web_components']['results']['mappings'] = [
    [
      'keys' => ['article-grid', 'page-grid', 'recipe-grid'],
      'element' => 'search-result-element-default',
      'settings' => '',
    ],
    [
      'keys' => ['recipe'],
      'element' => 'search-result-recipe',
      'settings' => [
        'showImage' => TRUE,
      ],
    ],
    [
      'keys' => ['default'],
      'element' => 'search-result-element-rendered',
      'settings' => [
        'field' => 'result',
      ],
    ],
  ];

  $endpoint->set('third_party_settings', $config);
  $endpoint->save();

  /** @var \Drupal\layout_builder\Section[] $sections */
  $sections = $searchNode->get('layout_builder__layout')->getValue();
  foreach ($sections as &$section) {
    $components = $section['section']->getComponents();
    foreach ($components as $component) {
      $plugin_id = $component->getPluginId();

      if ($plugin_id === 'swc_search_simple_pager') {
        $configuration = $component->get('configuration');
        $configuration['showFirstLast'] = TRUE;
        $component->setConfiguration($configuration);
        $section['section']->removeComponent($component->getUuid());
        $section['section']->appendComponent($component);
      }
    }

    if ($section['section']->getLayoutId() === 'search_web_components_twocol') {
      $firstComponents = [
        [
          'region' => 'first',
          'config' => [
            'id' => 'swc_facet_dropdown_html',
            'label' => 'Search Component: Content Type HTML Dropdown Facet',
            'label_display' => FALSE,
            'provider' => 'search_web_components',
            'endpoint' => 'content',
            'facet' => 'type',
            'key' => NULL,
            'preferAttributes' => NULL,
            'overrideLabel' => NULL,
            'showCount' => NULL,
            'showLabel' => NULL,
            'showReset' => NULL,
            'resetText' => NULL,
            'collapsible' => NULL,
            'closed' => NULL,
            'showCountInCollapseLabel' => NULL,
            'multipleSelect' => NULL,
            'htmlSelectLabel' => NULL,
            'required' => NULL,
          ],
        ],
        [
          'region' => 'first',
          'config' => [
            'id' => 'swc_facet_checkbox',
            'label' => 'Search Component: Cooking Time Checkbox Facet',
            'label_display' => FALSE,
            'provider' => 'search_web_components',
            'endpoint' => 'content',
            'facet' => 'cooking_time',
            'key' => NULL,
            'preferAttributes' => NULL,
            'overrideLabel' => NULL,
            'showCount' => NULL,
            'showLabel' => NULL,
            'showReset' => NULL,
            'resetText' => NULL,
            'collapsible' => NULL,
            'closed' => NULL,
            'showCountInCollapseLabel' => NULL,
            'useRadios' => NULL,
          ],
        ],
        [
          'region' => 'first',
          'config' => [
            'id' => 'swc_facet_checkbox',
            'label' => 'Search Component: Difficulty Checkbox Facet',
            'label_display' => FALSE,
            'provider' => 'search_web_components',
            'endpoint' => 'content',
            'facet' => 'difficulty',
            'key' => NULL,
            'preferAttributes' => NULL,
            'overrideLabel' => NULL,
            'showCount' => NULL,
            'showLabel' => NULL,
            'showReset' => NULL,
            'resetText' => NULL,
            'collapsible' => NULL,
            'closed' => NULL,
            'showCountInCollapseLabel' => NULL,
            'useRadios' => NULL,
          ],
        ],
        [
          'region' => 'first',
          'config' => [
            'id' => 'swc_facet_checkbox',
            'label' => 'Search Component: Servings Checkbox Facet',
            'label_display' => FALSE,
            'provider' => 'search_web_components',
            'endpoint' => 'content',
            'facet' => 'servings',
            'key' => NULL,
            'preferAttributes' => NULL,
            'overrideLabel' => NULL,
            'showCount' => NULL,
            'showLabel' => NULL,
            'showReset' => NULL,
            'resetText' => NULL,
            'collapsible' => NULL,
            'closed' => NULL,
            'showCountInCollapseLabel' => NULL,
            'useRadios' => NULL,
          ],
        ],
        [
          'region' => 'first',
          'config' => [
            'id' => 'uswca_search_example',
            'label' => 'Umami Search Component: Example',
            'label_display' => FALSE,
            'displayText' => 'This is some example display text',
          ],
        ],
      ];

      $secondComponents = [
        [
          'uuid' => \Drupal::service('uuid')->generate(),
          'region' => 'secondSecond',
          'config' => [
            'id' => 'swc_search_results_switcher',
            'label' => 'Search Component: Results display switcher',
            'label_display' => FALSE,
            'options' => NULL,
          ],
        ],
      ];

      foreach ($firstComponents as $fc) {
        $c = new SectionComponent($sc['uuid'] ?? \Drupal::service('uuid')->generate(), 'first', $fc['config']);
        $section['section']->appendComponent($c);
      }

      foreach ($secondComponents as $sc) {
        $cc = new SectionComponent($sc['uuid'] ?? \Drupal::service('uuid')->generate(), $sc['region'], $sc['config']);
        $section['section']->appendComponent($cc);
      }
    }

    $searchNode->set('layout_builder__layout', $sections);
    $searchNode->save();
  }

  drupal_flush_all_caches();
}
